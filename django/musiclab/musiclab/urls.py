from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'musiclab.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	url(r'^mus/', include('music.urls',namespace="music")),
    url(r'^admin/', include(admin.site.urls)),
)
