import json
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q
from django.db.models import Avg
from music.models import Song, Mark
from django.core.paginator import Paginator
import datetime
#Main page
def index(request):
    latest_song_list = Song.objects.all().order_by('-pub_date')[:10]
    latest_mark_list = Mark.objects.all().order_by('-pub_date')[:10]
    context = {'latest_song_list': latest_song_list,
    'latest_mark_list':latest_mark_list,'sesUser':request.user}
     
    return render(request, 'music/index.html', context)

#Song details
def detail(request, song_id):
    try:
        user = request.user
        song = Song.objects.get(pk=song_id)
        user_marks = Mark.objects.filter(user_id=user.id)
        user_mark = user_marks.filter(song_id=song.id)[:1]
    except Song.DoesNotExist:
        raise Http404
    return render(request, 'music/detail.html', {'sesUser':request.user,'song': song,'average':song.average, "user_mark":user_mark})

#Serach page
def search_view(request):
    search=request.GET["search_text"];    
    song_title_list = Song.objects.filter(Q(title__icontains=search)).order_by('pub_date')
    song_artist_list = Song.objects.filter(Q(artist__icontains=search)).order_by('pub_date')
    song_album_list = Song.objects.filter(Q(album__icontains=search)).order_by('pub_date')
    
    context={"song_title_list":song_title_list,"search":search,"song_artist_list":song_artist_list,"song_album_list":song_album_list}
    return render(request, 'music/search.html', context)

#Best list page
def best_view(request):
    song_list = Song.objects.all().order_by('-average')[:10]
    return render(request, 'music/best.html', {"best":song_list,'sesUser':request.user})

#User page
def user_view(request):    
    return render(request, 'music/user.html', {"sesUser":request.user,"mark_list":Mark.objects.filter(user_id=request.user.id).order_by('-pub_date')})

def comments_view(request,id):     
    song = Song.objects.get(pk = id)
    marks_list = Mark.objects.filter(song_id = id)
    pag = Paginator(marks_list,10)
    return render(request,'music/comments.html',{'sesUser':request.user,"song":song,"pages":pag.num_pages,"count":pag.count})

def comments_list(request):
    id = int(request.POST.get('song_id'))
    amount = int(request.POST.get('amount'))
    n_page = int(request.POST.get('page'))
    marks_list = Mark.objects.filter(song_id=id)
    pag = Paginator(marks_list, amount)
    response_data = {}
    response_data["list"]=[]
    for mark in pag.page(n_page).object_list:
        m = {
        "mark":mark.mark,
        "comment":mark.comment,
        "user_id":mark.user.id,
        "date": convertDatetimeToString(mark.pub_date),
        "username":mark.user.username
        }
        response_data["list"].append(m)
    response_data["total"] = pag.count
    response_data["pages"] = pag.num_pages
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def convertDatetimeToString(o):
	DATE_FORMAT = "%Y-%m-%d" 
	TIME_FORMAT = "%H:%M:%S"

	if isinstance(o, datetime.date):
	    return o.strftime(DATE_FORMAT)
	elif isinstance(o, datetime.time):
	    return o.strftime(TIME_FORMAT)
	elif isinstance(o, datetime.datetime):
	    return o.strftime("%s %s" % (DATE_FORMAT, TIME_FORMAT))
#AJAX user-edit
def edit_user(request):
    user = User.objects.get(pk=request.user.id)
    new_fname = request.POST.get("first_name")
    new_lname = request.POST.get("last_name")
    new_email = request.POST.get("email_name")
    response_data = {}
    if new_fname is not None:
        user.first_name = new_fname       
    if new_lname is not None:
       user.last_name = new_lname
    if new_email is not None:
        try:
            test_user = User.objects.get(email=new_email);
            response_data["reuslt"] = "false"
            response_data["cause"] = "email exist"
        except:
            user.email = new_email
    user.save()
    return HttpResponse(json.dumps({"result": "OK"}), content_type="application/json")

#AJAX Rate song
def vote(request):
    p = get_object_or_404(Song, pk=int(request.POST["song_id"]))
    user = request.user
    user_marks = Mark.objects.filter(user_id=user.id)
    userMark = int(request.POST['userMark'])
    userComment = request.POST['userComment']
    newMark = Mark(mark=userMark, comment=userComment)
    response_data = {}
    try:
        user_song = user_marks.filter(song_id=p.id)[0]        
        user_song.delete()
    except:
        response_data['first']="true"
    newMark.song = p
    newMark.user = user        
    newMark.save()
    p.average = p.mark_set.aggregate(Avg('mark')).values()[0]
    p.save()
    response_data['average'] = p.average
    response_data['comment'] = newMark.comment
    response_data['username'] = user.username
    response_data['mark'] = userMark
    response_data['new_id'] = newMark.id
    return HttpResponse(json.dumps(response_data), content_type="application/json")

#AJAX-login
def login_view(request):
    password = request.POST['password']
    name = request.POST['login']
    us = authenticate(username=name, password=password)
    response_data = {}
    if us is not None:
        login(request, us)
        response_data['result'] = 'success'
        response_data['username'] = us.username
        response_data['user_id'] = us.id
    else:
        response_data['result'] = 'fail'        
    return HttpResponse(json.dumps(response_data), content_type="application/json")

#AJAX-register
def register(request):
    uEmail = request.POST['email']
    uNickname = request.POST['nickname']
    uPassword = request.POST['password']
    response_data = {}
    try:
        tstUser = User.objects.get(email=uEmail)    
        response_data['result'] = 'error'
        response_data['message'] = 'E-mail already exist!'
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    except:
        us =  User.objects.create_user(uNickname,uEmail,uPassword)        
        us.save()
        response_data['result'] = 'success'
        return HttpResponse(json.dumps(response_data), content_type="application/json")

#AJAX Logout
def logout_view(request):
    logout(request)
    response_data = {}
    response_data['result'] = 'success'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

"""Three bellow not actual!"""
def user_top(request):
        return render(request, 'music/user.html', {"sesUser":request.user,"mark_list":Mark.objects.filter(user_id=request.user.id).order_by('-pub_date')})

def user_bottom(request):
    return render(request, 'music/user.html', {"sesUser":request.user,"mark_list":Mark.objects.filter(user_id=request.user.id).order_by('-pub_date')})

def user_last(request):
    return render(request, 'music/user.html', {"sesUser":request.user,"mark_list":Mark.objects.filter(user_id=request.user.id).order_by('-pub_date')})