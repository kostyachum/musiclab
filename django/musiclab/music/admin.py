from django.contrib import admin
from music.models import Song, Mark
# Register your models here.
admin.site.register(Song)
admin.site.register(Mark)

