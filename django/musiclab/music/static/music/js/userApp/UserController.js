/*
 * Приложение страницы пользователя
 * @type @exp;Base@call;extend
 */
musiclab.controller.UserController = Base.extend({
    fnameEdit: false,
    snameEdit: false,
    emailEdit: false,
    /*
     * Констуртор
     */
    constructor: function() {
        console.log("-- User Constructor")
        var me = this;
        me.doDynamic();

    },
    /*
     * Обработка динамических элементов
     */
    doDynamic: function() {
        var me = this;

        $("#editFirstName").click(function() {
            if (me.fnameEdit) {
                $("#firstNameEdior").hide("fast");
                me.fnameEdit = false;
            } else {
                $("#firstNameEdior").show("fast");
                me.fnameEdit = true;
            }

        });

        $("#editSecondName").click(function() {
            if (me.snameEdit) {
                $("#secondNameEdior").hide("fast");
                me.snameEdit = false;
            } else {
                $("#secondNameEdior").show("fast");
                me.snameEdit = true;
            }

        });

        $("#editEmail").click(function() {
            if (me.emailEdit) {
                $("#emailEdior").hide("fast");
                me.emailEdit = false;
            } else {
                $("#emailEdior").show("fast");
                me.emailEdit = true;
            }
        });

        var err = function(data) {
            console.log("Error:", data);
        };

        $("#doneFirstName").click(function() {
            var fnameSucc = function() {
                $("#userFNameHolder").hide("fast");
                $("#userFNameHolder").html($("input[name=first_name]").val());
                $("#userFNameHolder").show("fast");
                $("#firstNameEdior").hide("fast");

            };

            var newFName = $("#firstName").serialize();
            me.doEdit(newFName, fnameSucc, err);
            return false;
        });

        $("#doneLastName").click(function() {
            var lnameSucc = function() {
                $("#userLNameHolder").hide("fast");
                $("#userLNameHolder").html($("input[name=last_name]").val());
                $("#userLNameHolder").show("fast");
                $("#secondNameEdior").hide("fast");

            };
            var newFName = $("#lastName").serialize();

            me.doEdit(newFName, lnameSucc, err);
            return false;
        });

        $("#doneEmail").click(function() {
            var emailSucc = function() {
                $("#emailHolder").hide("fast");
                $("#emailHolder").html($("input[name=email]").val());
                $("#emailHolder").show("fast");
                $("#emailEdior").hide("fast");

            };
            var newFName = $("#email").serialize();
            me.doEdit(newFName, emailSucc, err);
            return false;
        });
    },
    doEdit: function(editData, succ, err) {
        $.ajax({
            type: 'post',
            async: 'true',
            url: edit_user_adress,
            data: editData,
            dataType: "json",
            error: err,
            success: succ
        });
    }
});