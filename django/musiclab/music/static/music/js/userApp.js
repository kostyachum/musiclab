/**
 * Старт.
 * Запуск приложения после загрузки странички
 */
$(document).ready(function () {
	
	/*
	 * стартуем ...
	 */
	try {
		var userController = new musiclab.controller.UserController();
	}
	catch (e) {
		alert("Exception:\n\n" + e);
	}
});