/**
 * Приложение навигацоной панели
 * @lends musiclab.controller.Controller
 */
musiclab.controller.Controller = Base.extend({
    util: new musiclab.controller.Util(),
    /*
     * Конструктор
     */
    constructor: function() {
        console.log("-- Controller: Конструктор.");
        var me = this;

        $("#modelRegister").modal();
        $("#modelRegister").modal('hide');
        $("#hideModel").show();

        console.log(USERNAME);

        if (USERNAME !== "") {
            $(LOGIN_FORM).hide();
            $(USER_HOLDER).show();
        }
        ;
        me.util.floatToStars(".rateHolder");
        console.log("just rated");
        me.doDynamic();
    },
    /*
     * Динамические элементы
     */
    doDynamic: function() {
        var me = this;

        // Loigin button
        $(LOGIN_BUTTON).click(function() {
            console.log("login to addres:" + login_addres);

            var succ = function(data) {
                console.log("SUCCESS: ");
                console.log(data);
                if (data.result === "success") {
                    $(USER_BUTTON).html(data.username);
                    USERNAME = data.username;
                    USER_ID = data.user_id
                    $(LOGIN_FORM).hide("slow");
                    $(USER_HOLDER).show("fast");
                    $("#userRate").show("fast");
                    $("#dublicate").show("fast");
                    $("#userComment").show("fast");
                    $("#loginPlease").hide("fast");

                } else {
                    alert(data.result);
                }
            };

            var err = function(data) {
                console.log("ERROR ");
                console.log(data);
                alert("Error login!");
            };

            logData = $(LOGIN_FORM).serialize();
            me.doLogin(logData, succ, err);
            return false;
        });
        // Registration button
        $(REG_BUTTON).click(function() {
            $("#modelRegister").modal('show');
            return false;
        });

        // Cancel registration button
        $(CANCEL_BUTTON).click(function() {
            $("#modelRegister").modal('hide');

            return false;
        });

        // Send registration query
        $(DO_REG_BUTTON).click(function() {
            $("#errorRegister").hide();
            var error = function(data) {
                console.log("error: " + data.message);
                console.log(data);
            };

            var succ = function(data, err) {
                console.log("succ: " + data);
                if (data.result === "error") {
                    $("#errorMessage").html(data.message);
                    $("#errorRegister").show();
                    alert(err);
                } else {
                    $("#modelRegister").modal('hide');
                    $("#loginHolder").val($("#nick").val());
                }
                console.log(data);
            };

            if (me.checkReg() === true) {
                var regData = $("#regForm").serialize();
                me.doRegistration(regData, succ, error);
            } else {
                $("errorRegister").show();
            }
        });

        // Send logout query
        $(LOGOUT_BUTTON).click(function() {
            var succ = function(data) {
                console.log(data);
                $(USER_HOLDER).hide("fast");
                $(LOGIN_FORM).show("fast");
                $("#userRate").hide("fast");
                $("#dublicate").hide("fast");
                $("#userComment").hide("fast");
                $("#loginPlease").show("fast");
                USERNAME = "";
            };
            var err = function(data) {
                console.log(data);
            };
            var data = $(LOGOUT_FORM).serialize();

            me.doLogout(data, succ, err);
        });
    },
    /*
     * Проверка полей регистарции
     */
    checkReg: function() {
        var email = $("#email").val();
        var nick = $("#nick").val();
        var password = $("#pass").val();
        var rePassword = $("#rePass").val();

        var checked = true;

        if (email === "" || email === undefined) {
            checked = false;
            $("#emailGroup").addClass("error");
        } else {
            $("#emailGroup").removeClass("error");
        }

        if (nick === "" || nick === undefined) {
            checked = false;
            $("#nickGroup").addClass("error");
        } else {
            $("#nickGroup").removeClass("error");
        }

        if (password !== rePassword) {
            checked = false;
            $("#rePassGroup").addClass("error");
        } else {
            $("#rePassGroup").removeClass("error");
        }

        if (password === "" || password === undefined) {
            checked = false;
            $("#passGroup").addClass("error");
        } else {
            $("#passGroup").removeClass("error");
        }

        if (rePassword === "" || rePassword === undefined) {
            checked = false;
            $("#rePassGroup").addClass("error");
        } else {
            $("#rePassGroup").removeClass("error");
        }

        return checked;
    },
    /*
     * AJAX-login
     */
    doLogin: function(logData, succ, err) {
        $.ajax({
            type: 'post',
            async: 'true',
            url: login_addres,
            data: logData,
            dataType: "json",
            error: err,
            success: succ
        });
    },
    /**
     * AJAX-register
     */
    doRegistration: function(regData, succ, err) {
        $.ajax({
            type: 'post',
            async: 'true',
            url: reg_addres,
            data: regData,
            dataType: "json",
            error: err,
            success: succ
        });
    },
    /**
     * AJAX-logout
     * 
     */
    doLogout: function(data, succ, err) {
        $.ajax({
            type: 'GET',
            async: 'true',
            url: logout_addres,
            data: data,
            dataType: "json",
            error: err,
            success: succ
        });
    }
});