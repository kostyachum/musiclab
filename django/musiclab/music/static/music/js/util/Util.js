/**
 * 
 * @lends musiclab.controller.Controller
 */
musiclab.controller.Util = Base.extend({
    constructor: function() {
        console.log("-- Util: Конструктор.");
        var me = this;

    },
    floatToStars: function(markHolder) {
        $(markHolder).each(function(a, b) {
            var averageMark = parseFloat(b.innerHTML);
            var tmpStar = "";
            for (var i = 1; i < 6; i++) {
                var di = averageMark - i;
                // console.log(di);
                tmpStar += "<input name ='aveMark"+a+"' type='radio' class='star' disabled='disabled' ";
                if (di >= -0.5 && di <= 0.5) {
                    tmpStar += " checked='checked'";
                }
                tmpStar += ">";
            }

            b.innerHTML=tmpStar;
        });
        $(".star").rating();
    }
});