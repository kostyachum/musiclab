/**
 * Приложение страцы песни
 * @lends musiclab.controller.Controller
 */
musiclab.controller.DetailController = Base.extend({
    /*
     * Конструктор
     */
    constructor: function() {
        console.log("-- DetailController: Конструктор.");
        var me = this;
        console.log(SONG_ID);
        console.log("trarara:", USERMARK.length);
        //Если есть коментарии
        if (USERMARK.length > 3) {
            $("#userRate").hide();
            $("#dublicate").show();
        } else {
            $("#userRate").show();
            $("#dublicate").hide();
        }


        if (USERNAME) {
            $("#loginPlease").hide();
        } else {
            $("#userRate").hide();
        }
        ;

        /*
         * JPLayer
         */
        console.log("start");

        $("#jquery_jplayer_1").jPlayer({
            ready: function() {
                $(this).jPlayer("setMedia", {
                    mp3: "https://dl.dropboxusercontent.com/u/15968905/audio/Dropkick%20Murphys%20-%20Fields%20Of%20Athenry.mp3",
                });
                console.log("ready!");
            },
            swfPath: swfPathStatic,
            supplied: "mp3"
        });

        me.doDynamic();
    },
    doDynamic: function() {
        var me = this;
        //Подтверждние замены коментария
        $("#remarkButton").click(function() {
            $("#userRate").show("slow");
            $("#dublicate").hide("fast");
        });

        //Кнопка отправки оценки
        $(RATE_BUTTON).click(function() {
            me.rateThis();
            return false;
        });

        $(".rateMe").each(function(a, b) {
            var mrk = parseInt(b.innerHTML);
            b.innerHTML = "";

            for (var i = 1; i < 6; i++) {
                var tmp = "";
                tmp += "<input type='radio' class='star' name='votes" + a + "' disabled='disabled'";

                if (i === mrk) {
                    tmp += "checked='checked'";
                }
                tmp += " />";
                b.innerHTML += tmp;
                //  console.log(tmp);
            }
        });

        me.floatToStars();
        $(".star").rating();
    },
    /*
     * Отправка запроса голосования
     */
    doVote: function(voteData, succ, err) {
        $.ajax({
            type: 'post',
            async: 'true',
            url: vote_addres,
            data: voteData,
            dataType: "json",
            error: err,
            success: succ
        });
    },
    /*
     * Перевод численных оценок в звезды
     */
    floatToStars: function() {
        var averageMark = parseFloat($(AVERAGE_HOLDER).html());
        var tmpStar = "";
        console.log("ave:" + averageMark);
        for (var i = 1; i < 6; i++) {
            var di = averageMark - i;
            // console.log(di);
            tmpStar += "<input name ='aveMark' type='radio' class='star' disabled='disabled' ";
            if (di >= -0.5 && di <= 0.5) {
                tmpStar += " checked='checked'";
            }
            tmpStar += ">";
        }

        $(AVERAGE_HOLDER).html(tmpStar);
        $(".star").rating();
    },
    /*
     * Обработка голосования
     */
    rateThis: function() {
        var me = this;
        console.log("RATE IT!");
        var userMark = $('input[name=mark]:checked', '#rateForm').val()
        var userComment = $('textarea[name=userComment]', '#rateForm').val()

        $("#hiddenMark").val(userMark);
        $("#hiddenComment").val(userComment);
        $("#hiddenSong").val(SONG_ID);


        var voteData = $("#rateFormHidden").serialize();
        console.log(voteData);

        var succ = function(data) {
            $("#averageHolder").html(data.average);
            $("#rateForm").hide("slow");
            $("#dublicate").hide("fast");
            $("#userMark").html(data.mark);
            $("#userComment").html(data.comment);
            if (USERMARK.length < 3) {
                $("#thankYou").show("slow");
            }
            SONG_ID = data.new_id;
            me.floatToStars();
            console.log(data);
        };

        var err = function(data) {
            alert("Nope!");
            console.log(data);
        };

        me.doVote(voteData, succ, err);
        return false;
    }
});