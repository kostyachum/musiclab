/**
 * Приложение страцы песни
 * @lends musiclab.controller.Controller
 */
musiclab.controller.CommentsController = Base.extend({
    util: new musiclab.controller.Util(),
    /*
     * Конструктор
     */
    constructor: function() {
        console.log("-- CommentsController: Конструктор.");
        var me = this;
        console.log("song_id: ", SONG_ID);
        var token = $.parseHTML(NOT_FORBIDDEN);
        var tkn_name = $(token).attr('name');
        var tkn_val = $(token).val();
        var first_load_data = {
            "page": 1,
            "amount": 10,
            "song_id": SONG_ID
        };
        first_load_data[tkn_name] = tkn_val;
        console.log(first_load_data);

        var succ = function(data) {
            me.renderComments(data);
        };
        var err = function(data, c) {
            console.log("error: ", data, c);
        };

        me.loadPage(first_load_data, succ, err);
        me.doDynamic();
    },
    doDynamic: function() {
        var me = this;
        //Подтверждние замены коментария

    },
    renderComments: function(data) {
        var me = this;
        var pages = data.pages;
        var paginataionHTML = "<ul>";
        var comments = "";
        /*
         paginataionHTML += "<li><a href='#'>Prev</a></li>";
         for (var i = 1; i <= pages; i++) {
         paginataionHTML += "<li><a href='#'>" + i + "</a></li>";
         }
         
         paginataionHTML += "<li><a href='#'>Next</a></li>";
         paginataionHTML += "</ul>"
         */
        $("#paginationHolder").html(paginataionHTML);
        console.log("length:", data.list.length);
        for (var i = 0; i < data.list.length; i++) {
            comments += "<div class='span4'>\n";
            comments += "<span class='username'>"+data.list[i].username+"</span>\n";
            comments += "<span class='pubTime'>" + data.list[i].date + "</span>\n";
            comments += "<div class='someRating'>" + data.list[i].comment + "<br /></div>\n";
            comments += "<div class='starHolder'> <span class='rateMe' >" + data.list[i].mark + "</span> </div>\n";
            comments += "</div>\n\n";
        }

        $("#commentHolder").html("");
        $("#commentHolder").html(comments);

        me.util.floatToStars(".rateMe");
        $(".star").rating();
    },
    /*
     * Перевод численных оценок в звезды
     */
    floatToStars: function() {
        var averageMark = parseFloat($(AVERAGE_HOLDER).html());
        var tmpStar = "";
        console.log("ave:" + averageMark);
        for (var i = 1; i < 6; i++) {
            var di = averageMark - i;
            // console.log(di);
            tmpStar += "<input name ='aveMark' type='radio' class='star' disabled='disabled' ";
            if (di >= -0.5 && di <= 0.5) {
                tmpStar += " checked='checked'";
            }
            tmpStar += ">";
        }

        $(AVERAGE_HOLDER).html(tmpStar);
        $(".star").rating();
    },
    loadPage: function(data, succ, err) {
        $.ajax({
            type: 'post',
            async: 'true',
            url: comments_list_adress,
            data: data,
            dataType: "json",
            error: err,
            success: succ
        });
    }
});