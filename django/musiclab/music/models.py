from django.db import models
from django.contrib.auth.models import User


class Song(models.Model):
	id = models.AutoField(primary_key=True)
	title =  models.CharField(max_length=200)
	artist =  models.CharField(max_length=200)
	album =  models.CharField(max_length=200)
	picture =  models.CharField(max_length=200, default="/static/music/img/No_Cover.gif")
	link =  models.CharField(max_length=200, default="default")
	pub_date = models.DateTimeField('date published', auto_now_add=True)
        average = models.FloatField(default=0);
        def __unicode__(self):  # Python 3: def __str__(self):
		return '%s: %s ' % (self.artist, self.title)

class Mark(models.Model):
	id = models.AutoField(primary_key=True)
	mark = models.IntegerField()
	comment = models.CharField(max_length=512)
	pub_date = models.DateTimeField('date published',  auto_now_add=True)
	song = models.ForeignKey(Song)
	user = models.ForeignKey(User)