from django.conf.urls import patterns, url

from music import views

urlpatterns = patterns('',
   # ex: /mus/
    url(r'^$', views.index, name='index'),
    # ex: /mus/5/
    url(r'^(?P<song_id>\d+)/$', views.detail, name='detail'),
    # ex: /mus/5/comments
    url(r'^(?P<id>\d+)/comments/$', views.comments_view, name='comments_view'),
    url(r'^comments_list/$', views.comments_list, name='comments_list'),
    # ex: /mus/5/vote/
    url(r'^search/$', views.search_view, name='search_view'),
    url(r'^best/$', views.best_view, name='best_view'),
    url(r'^user/$', views.user_view, name='user_view'),
    # AJAX
    url(r'^vote/$', views.vote, name='vote'),
    url(r'^login/$', views.login_view, name='login_view'),
    url(r'^register/$', views.register, name='register'),
    url(r'^logout/$', views.logout_view, name='logout_view'),
    url(r'^user/edit$', views.edit_user, name='edit_user'),
)
