/**
 * Старт.
 * Запуск приложения после загрузки странички
 */
$(document).ready(function () {
	
	/*
	 * стартуем ...
	 */
	try {
		var controller = new musiclab.controller.Controller();
	}
	catch (e) {
		alert("Exception:\n\n" + e);
	}
});