/**
 * Это класс управления процессами Приложения.
 * @lends musiclab.controller.Controller
 */
musiclab.controller.Controller = Base.extend({
    constructor: function() {
        console.log("-- Controller: Конструктор.");
        var me = this;
        me.doDynamic();
    },
    doDynamic: function() {
        $(LOGIN_BUTTON).click(function() {
            console.log("do login");
        });
        
        $(REG_BUTTON).click(function() {
            console.log("do reg");
        });
    }
});