<%-- 
    Document   : index
    Created on : 06.12.2013, 15:52:21
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
        
        <!-- Весь JS собираеться в один файл-->
        <script type="text/javascript" src="js/Base.js"></script>
        <script type="text/javascript" src="js/lib/namespace.js"></script>
        <script type="text/javascript" src="js/indexApp.js"></script>
        <script type="text/javascript" src="js/indexApp/Controller.js"></script>
        
        
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="/header.jsp" />

        <h1>Best song list:</h1>
        <jsp:include page="/IndexServlet" /> 
        <hr />

        <jsp:include page="/footer.jsp" />
    </body>

    <script>
        LOGIN_BUTTON = "#loginButton";
        REG_BUTTON = "#regButton";
    </script>
</html>
