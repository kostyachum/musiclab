package util;

import musiclab.model.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory object.
 *
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            AnnotationConfiguration ac = new AnnotationConfiguration();
            //Добавление аннотированных классов сущностей.
            ac.addAnnotatedClass(User.class);
            ac.addAnnotatedClass(Mark.class);
            ac.addAnnotatedClass(Song.class);

            sessionFactory = ac.configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
  
    public static Session getSession()
            throws HibernateException {
        return sessionFactory.openSession();
    }
}
