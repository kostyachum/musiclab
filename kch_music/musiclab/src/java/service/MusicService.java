/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dao.GenericDAO;
import dao.impl.ImplementionDAO;
import musiclab.model.*;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.util.Collection;

/**
 *
 * @author user
 */
public class MusicService {

    private static final MusicService INST = new MusicService();

    GenericDAO<User, Long> userDAO;
    GenericDAO<Mark, Long> markDAO;
    GenericDAO<Song, Long> songDAO;

    private MusicService() {
        this.songDAO = new ImplementionDAO(Song.class);
        this.userDAO = new ImplementionDAO(User.class);
        this.markDAO = new ImplementionDAO(Mark.class);
    }

    public static MusicService getInstance() {
        return INST;
    }

    /**
     * Получение пользователя по имени (имейлу)
     *
     * @param name
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public User getUserByName(String name) throws ClassNotFoundException, SQLException, Exception {
        return userDAO.getUserByName(name);
    }

    /**
     * Получение пользователя по имени (имейлу)
     *
     * @param name
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public User getUserById(Long id) throws ClassNotFoundException, SQLException, Exception {
        return userDAO.getById(id);
    }

    /**
     * Добавить пользователя
     *
     * @param user
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public void addUser(User user) throws ClassNotFoundException, SQLException, Exception {
        userDAO.add(user);
    }

    /**
     * Изменить пользователя
     *
     * @param user
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public void updateUser(User user) throws ClassNotFoundException, SQLException, Exception {
        userDAO.update(user);
    }

    /**
     * Получить песню по идентификатору
     *
     * @param id
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public Song getSongById(Long id) throws ClassNotFoundException, SQLException, Exception {
        return songDAO.getById(id);
    }

    /**
     * Найти песню по заданому объекту
     *
     * @param song
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public Collection<Song> searchSongList(Song song) throws ClassNotFoundException, SQLException, Exception {
        return songDAO.searchSong(song);
    }

    /**
     * Список лучше всего оценёных песен
     *
     * @param restrict
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public Collection<Song> getBestSongList(int restrict) throws ClassNotFoundException, SQLException, Exception {
        return songDAO.getBestSongList(restrict);
    }

    /**
     * Список лучших песен пользователя
     *
     * @param restrict
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public Collection<Song> getBestUserSongList(int restrict) throws ClassNotFoundException, SQLException, Exception {
        return songDAO.getBestSongList(restrict);
    }

    /**
     * Добавить оценку
     *
     * @param mark
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public void addMark(Mark mark) throws ClassNotFoundException, SQLException, Exception {
        markDAO.add(mark);
    }

    /**
     * Получить список оценок
     *
     * @param restrict количество
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public Collection<Mark> getMarkList(int restrict) throws ClassNotFoundException, SQLException, Exception {
        return markDAO.getMarkList(restrict);
    }

    /**
     * Получить список оценок пользователя
     *
     * @param user
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public Collection<Mark> getUserMarkList(User user) throws ClassNotFoundException, SQLException, Exception {
        return markDAO.getUserMarkList(user);
    }
    
    /**
     * Получение дайджеста строки.
     *
     * @param password
     * @return
     * @throws Exception
     */
    public String getPasswordDigest(String password) throws Exception {
        String pwd;
        byte[] pwdByteArray;
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(password.getBytes());
        pwdByteArray = md.digest();
        pwd = "";
        for (int i = 0; i < pwdByteArray.length; i++) {
            pwd += (new Byte(pwdByteArray[i])).intValue();
        }
        return pwd;
    }   
}
