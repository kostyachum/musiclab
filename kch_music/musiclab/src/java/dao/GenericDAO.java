/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import musiclab.model.Mark;
import musiclab.model.Song;
import musiclab.model.User;

/**
 *
 * @author user
 */
public interface GenericDAO<T, K extends Serializable> {

    /**
     * Добавление объекта
     *
     * @param entity
     * @param con объект соединения с бд
     * @throws Exception
     */
    public void add(T entity) throws Exception;

    /**
     * Удаление объекта
     *
     * @param entity
     * @param con объект соединения с бд
     * @throws Exception
     */
    public void delete(T entity) throws Exception;

    /**
     * Изменение
     *
     * @param entity
     * @param con объект соединения с бд
     * @throws Exception
     */
    public void update(T entity) throws Exception;

    /**
     * Получение объекта по идентификатору
     *
     * @param id
     * @param con объект соединения с бд
     * @return T объект
     * @throws Exception
     */
    public T getById(K id) throws Exception;

    /**
     * Получение всех оценок пользователя
     *
     * @param user
     * @param con объект соединения с бд
     * @return Collection<Mark> список оценок
     * @throws Exception
     */
    public Collection<Mark> getUserMarks(User user) throws Exception;

    /**
     * Получение самых высоких оценок пользователя
     *
     * @param user
     * @param con объект соединения с бд
     * @return Collection<T> список оценок
     * @throws Exception
     */
    public Collection<Song> getUserBest(User user) throws Exception;

    /**
     * Получение оценок песни
     *
     * @param song
     * @param con объект соединения с бд
     * @return Collection<Mark> список оценок
     * @throws Exception
     */
    public Collection<Mark> getSongMarks(Song song) throws Exception;

    /**
     * Поиск песни
     *
     * @param song
     * @param con объект соединения с бд
     * @return Collecttion<Song> найденные объекты
     * @throws Exception
     */
    public Collection<Song> searchSong(Song song) throws Exception;

    /**
     * Пользователь по имени
     *
     * @param name
     * @param con объект соединения с бд
     * @return
     */
    public User getUserByName(String name) throws Exception;

    /**
     * Список лучших песени вообще
     *
     * @param restrict
     * @param con
     * @return
     */
    public Collection<Song> getBestSongList(int restrict) throws Exception;

    public Collection<Mark> getMarkList(int restrict) throws Exception;

    public Collection<Mark> getUserMarkList(User user) throws Exception;
}
