package dao.impl;

import dao.GenericDAO;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import musiclab.model.Mark;
import musiclab.model.Song;
import musiclab.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import util.HibernateUtil;

/**
 *
 * @author user
 */
public class ImplementionDAO<T, K extends Serializable> implements GenericDAO<T, K> {

    private Class<T> type;

    /**
     * Конструктор
     *
     * @param type
     * @throws Exception
     */
    public ImplementionDAO(Class<T> type) {
        this.type = type;
        System.out.println("Type from constr: " + type);
    }

    @Override
    public void add(T entity) throws Exception {
        Session session = createSession();
        session.save(entity);
        closeSession(session);
    }

    @Override
    public void delete(T entity) throws Exception {
        Session session = createSession();
        session.delete(entity);
        closeSession(session);
    }

    @Override
    public void update(T entity) throws Exception {
        Session session = createSession();
        session.saveOrUpdate(entity);
        closeSession(session);
    }

    @Override
    public T getById(K id) throws Exception {
        Session session = createSession();
        T entity = (T) session.get(type, id);
        closeSession(session);
        return entity;
    }

    @Override
    public Collection<Mark> getUserMarks(User user) throws Exception {
        Session session = createSession();
        Criteria cr = session.createCriteria(Mark.class).add(Restrictions.eq("user", user));
        Collection<Mark> list = cr.list();
        closeSession(session);
        return list;

    }

    @Override
    public Collection<Song> getUserBest(User user) throws Exception {
        Session session = createSession();
        //Пока только в title
        Criteria cr = session.createCriteria(Song.class).add(Restrictions.eq("user", user));
        cr.addOrder(Order.desc("average"));
        cr.setMaxResults(10);
        Collection list = cr.list();
        closeSession(session);
        return list;
    }

    @Override
    public Collection<Mark> getSongMarks(Song song) throws Exception {
        Session session = createSession();
        Criteria cr = session.createCriteria(Mark.class).add(Restrictions.eq("song", song));
        Collection<Mark> list = cr.list();
        closeSession(session);
        return cr.list();
    }

    @Override
    public Collection<Song> searchSong(Song song) throws Exception {
        Session session = createSession();
        //Пока только в title
        Collection<Song> songList = session.createQuery("FROM t_song WHERE title LIKE '%" + song.getTitle() + "%'").list();
        return songList;
    }

    @Override
    public User getUserByName(String name) throws Exception {
        Session session = createSession();
        Criteria cr = session.createCriteria(User.class);
        cr.add(Restrictions.eq("name", name));
        List res = cr.list();
        User answer = null;
        if (res.size() > 0) {
            answer = (User) res.get(0);
        }
        closeSession(session);
        return answer;
    }

    @Override
    public Collection<Song> getBestSongList(int restrict) throws Exception {
        Session session = createSession();
        //Пока только в title
        Criteria cr = session.createCriteria(Song.class);
        cr.addOrder(Order.desc("average"));
        cr.setMaxResults(restrict);
        Collection<Song> list = cr.list();
        closeSession(session);
        return list;
    }

    @Override
    public Collection<Mark> getMarkList(int restrict) throws Exception {
        Session session = createSession();
        Criteria cr = session.createCriteria(Mark.class);
        Collection<Mark> list = cr.list();
        closeSession(session);
        return cr.list();
    }

    @Override
    public Collection<Mark> getUserMarkList(User user) throws Exception {
        Session session = createSession();
        Criteria cr = session.createCriteria(Mark.class).add(Restrictions.eq("user", user));
        Collection<Mark> list = cr.list();
        closeSession(session);
        return cr.list();
    }

    /**
     * Открытие сессии и начало транзакции
     *
     * @return
     */
    private Session createSession() {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();

        return session;
    }

    /**
     * Закрытие сессии и транзакцции
     *
     * @param session
     * @throws Exception
     */
    private void closeSession(Session session) throws Exception {
        Exception ex = null;
        try {
            session.getTransaction().commit();
        } catch (Exception e) {
            ex = e;
        } finally {
            if (session != null && session.isOpen()) {
                System.out.println("close in fin");
                session.flush();
                session.close();
                if (ex != null) {
                    throw ex;
                }
            }
        }
    }

}
