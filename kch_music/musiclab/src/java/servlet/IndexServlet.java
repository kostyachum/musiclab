package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


/**
 *
 * @author user
 */
@WebServlet(name = "IndexServlet", urlPatterns = {"/IndexServlet"})
public class IndexServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
         for (int i = 0; i < 10; i++) {                
              out.println("Artist #" + i+": Song #" + i+"<br>");
            }
         
         
        //out.close();
        /*
        JSONArray songList = new JSONArray();
        JSONObject json = new JSONObject();
        System.out.println("doGet start");
        try {
            for (int i = 0; i < 10; i++) {
                List row = new ArrayList();
                row.add("Artist #" + i);
                row.add("Song #" + i);
                songList.add(row);
            }

            json.put("songList", songList);

            System.out.println(json);
            PrintWriter pw = response.getWriter();
            response.setContentType("text/plain");
            pw.println("" + json.toString() + "");
            pw.flush();
            pw.close();
        } catch (Exception ex) {
            PrintWriter pw = response.getWriter();
            response.setContentType("text/plain");
            pw.println(ex.getMessage());
            pw.flush();
            pw.close();
        }*/
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
