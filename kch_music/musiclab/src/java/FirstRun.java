
import dao.GenericDAO;
import dao.impl.ImplementionDAO;
import musiclab.model.Song;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kostya
 */
public class FirstRun {
      public static void main(String args[]) throws Exception{
      GenericDAO<Song, Long> songDAO;
          songDAO = new ImplementionDAO<Song, Long>(Song.class);
          
          Song song = new Song();
          song.setAlbum("album");
          song.setArtist("artist");
          song.setLink("link");
          song.setTitle("title");
          songDAO.add(song);
    }
}
