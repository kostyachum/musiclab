package musiclab.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Entity;

/**
 *
 * @author user
 */
@Entity
@Table(name = "t_user")
public class User implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "digest")
    private String passwordDigest;

    @Column(name = "avatar")
    private String avatarLink;

    @Column(name = "nick")
    private String nick;

    @Column(name = "markList")
    @OneToMany(mappedBy = "user")
    private Set<Mark> markList;

    public User() {
        this.markList = new HashSet<Mark>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordDigest() {
        return passwordDigest;
    }

    public void setPasswordDigest(String passwordDigest) {
        this.passwordDigest = passwordDigest;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public void setAvatarLink(String avatarLink) {
        this.avatarLink = avatarLink;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Set<Mark> getMarkList() {
        return markList;
    }

    public void setMarkList(Set<Mark> markList) {
        this.markList = markList;
    }
}